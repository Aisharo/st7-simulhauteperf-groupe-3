import parallel_kangaroo as kang 
tune = []
optimisation_technique = []
params = {'compilationType': '-O3', 'makefileType': 'avx512', 'xNodeSize': '256', 'yNodeSize': '256', 'zNodeSize': '256', 'threading': '4', 'iteration': '100', 'xCacheBlocking': '32', 'yCacheBlocking': '32', 'zCacheBlocking': '32'}
paramsVariationStep = {'xNodeSize': 256, 'yNodeSize': 256, 'zNodeSize': 256, 'threading': 1, 'iteration': 1, 'xCacheBlocking': 16, 'yCacheBlocking': 4, 'zCacheBlocking': 4}
algoParameters ={'ts': 10, 'tmin': 3, 'tmax': 15, 'temp': 100, 'la': 0.85, 'sl': 2}
kang.searchLocalOptimum(tune,optimisation_technique,algoParameters, params, paramsVariationStep)