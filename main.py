import parallel_kangaroo as kang 
import launcherClean as lau
import subprocess as sp 
import argparse

#--------------------------------------------------------------------------
#Getting different arguments needed
#--------------------------------------------------------------------------

#default parameters
params = {"compilationType" : "-O3", "makefileType" : "avx512", "xNodeSize" : "256", "yNodeSize" : "256", "zNodeSize" : "256", "threading": "4", "iteration": "100", "xCacheBlocking" : "32", "yCacheBlocking" : "32", "zCacheBlocking" : "32"}
paramNames = params.keys()
paramsVariationStep = {"xNodeSize": 256, "yNodeSize": 256, "zNodeSize" : 256, "threading" : 1, "iteration": 1, "xCacheBlocking" : 16, "yCacheBlocking" : 4, "zCacheBlocking" : 4}
batchResaName = "resa7st7pro5"

#indices parameters to tune
tune = []

parser = argparse.ArgumentParser(description='Parallel Hillclimbing Optisation')

parser.add_argument("-c", "--compilationType", help="Compilation Optimisation to be used", action='store_true')
parser.add_argument("-m", "--makefileType", help="Actually I do not know what that parameters mean", action='store_true')
parser.add_argument("-XNS", "--xNodeSize", help="Size on the x axis of the tile", action='store_true')
parser.add_argument("-YNS", "--yNodeSize", help="Size on the y axis of the tile", action='store_true')
parser.add_argument("-ZNS", "--zNodeSize", help="Size on the z axis of the tile", action='store_true')
parser.add_argument("-t", "--threading", help="Number of threads", action='store_true')
parser.add_argument("-i", "--iteration", help="Number of iterations", action='store_true')
parser.add_argument("-XCB", "--xCacheBlocking", help="Size of x cache blocking", action='store_true')
parser.add_argument("-YCB", "--yCacheBlocking", help="Size of y cache blocking", action='store_true')
parser.add_argument("-ZCB", "--zCacheBlocking", help="Size of z cache blocking", action='store_true')
parser.add_argument('-o', '--optimisationTechnique', help= "Type d'optimisation disponible, pour le moment nous disponsons de greedy tabu tabuDynamic annealing (qui peut se combiner avec tabu xor greedy OR avec stage)", nargs='+', default=[])
parser.add_argument("-ts", '--tabuSize', help= "Size of the Tabu List, only useful if tabu in -o", default = 10)
parser.add_argument("-tmin", '--tabuMin', help= "Minimum size of the tabu list qhen using tabuDynamic", default = 3)
parser.add_argument("-tmax", '--tabuMax', help= "Maximum size of the tabu list qhen using tabuDynamic", default = 15)
parser.add_argument('-temp', '--initialTemperature', help= "initial temperature for simulated annealing", default = 100)
parser.add_argument("-la", "--temperatureEvolutionLaw", help= "T = T*la: temperature evolution law for simulated annealing", default= 0.85)
parser.add_argument("-sl", "--stageLength", help= "sl: Length of each simulated annealing stage", default= 2)
parser.add_argument("-nk", "--numberKangaroo", help="number of kangaroos", default = 4)


args=parser.parse_args()

for x in params.keys():
    if getattr(args,x):
        tune.append(x)

optimisation_technique = args.optimisationTechnique

if "kangaroo" in optimisation_technique:
    parallelBool = True
else:
    parallelBool = False

number_nodes = int(args.numberKangaroo)//2
number_process = args.numberKangaroo
algoParameters = {"ts" : int(args.tabuSize), "tmin" : int(args.tabuMin), "tmax" : int(args.tabuMax), "temp" : float(args.initialTemperature), "la" : float(args.temperatureEvolutionLaw), "sl" : int(args.stageLength)}


#####################################################################
# Generating python file to mpi run
#####################################################################

f = open("launch.py","w+")

f.write("import parallel_kangaroo as kang \n")
f.write("tune = " + str(tune) + "\n")
f.write("optimisation_technique = " + str(optimisation_technique) + "\n")
f.write("params = " + str(params) + "\n")
f.write("paramsVariationStep = " + str(paramsVariationStep) + "\n")
f.write("algoParameters =" + str(algoParameters) + "\n")
f.write("kang.searchLocalOptimum(tune,optimisation_technique,algoParameters, params, paramsVariationStep)")
f.close()

#####################################################################
# Generating bench file
#####################################################################

bench = open("bench","w+")

bench.write("#!/bin/sh \n")
bench.write("#SBATCH --time=10 \n")
bench.write("mpirun -np "+str(number_process)+" -map-by ppr:1:socket -bind-to socket python3 launch.py")

bench.close()

#####################################################################
# Launching bench
#####################################################################


if parallelBool:
    sp.run("chmod 700 bench", shell=True)
    res = sp.run("sbatch -N " + str(number_nodes) + '-n '+ str(number_process) + " --exclusive -p kyle --qos=max8 bench", shell=True, stdout = sp.PIPE)
else:
    res = sp.run("mpirun -np 1 -map-by ppr:1:socket -bind-to socket python3 launch.py", shell=True, stdout = sp.PIPE)


print(str(res.stdout.decode()))
