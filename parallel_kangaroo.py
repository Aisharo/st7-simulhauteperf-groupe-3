import mpi4py
from mpi4py import MPI
import random as rd
import math
import numpy as np
import time
import argparse

import launcherClean as lau
  
#Default Constant
kmax = 3

def searchLocalOptimum(tune,tuning_type, algoParameters, params, paramsVariationStep):


  #MPI information extraction
  comm = MPI.COMM_WORLD 
  NbP = comm.Get_size() # number of processes
  Me  = comm.Get_rank() # index of THIS process


  #Process 0 prints a msg displaying the number of processes.
  if Me == 0:

        print("PE: ", Me, "/",NbP,": all processes started")
        print("#####################################################")
        print(f"INITIAL MAKE CALLED: with {params['compilationType']} {params['makefileType']}")
        print("#####################################################")

        # compile one
        lau.compiling(params["compilationType"], params["makefileType"])


  # forms a barrier, no process proceeds until all processes arrive at the barrier
  comm.barrier()

  #Each process runs a local search method from a random starting point

  #--------------------------------------------------------------------------
  # Min & MAx values for each component of the solution
  #--------------------------------------------------------------------------

  # 5 elts in a solution (x,y,opt,nbth,disk)
  NbDim = len(tune)

  # minimum and maximum values for tuned fields
  lmin = {
    "xNodeSize": 1, 
    "yNodeSize": 1, 
    "zNodeSize" : 1, 
    "threading" : 1, 
    "iteration": 1,
    "xCacheBlocking": 1, 
    "yCacheBlocking": 1,
    "zCacheBlocking": 1,
  }

  lmax = {
    "xNodeSize": 10, 
    "yNodeSize": 10, 
    "zNodeSize" : 10, 
    "threading" : 8, 
    "iteration": 1000,
    "xCacheBlocking": 10, 
    "yCacheBlocking": 40, 
    "zCacheBlocking": 40,
  }

  #--------------------------------------------------------------------------
  # Random generation of the S0 solution of the process
  #--------------------------------------------------------------------------
  def generateS0(Me):
    SeedInc = 200
    rd.seed(Me+1000*(1+SeedInc))
    S0 = []
    # form an S0 starting point with random parameters
    for param in tune:
      S0.append(rd.randrange(lmin[param],lmax[param]+1,1))
    
    for param in tune:
      S0[tune.index(param)] = S0[tune.index(param)]*paramsVariationStep[param]

    return(S0)

  S0 = generateS0(Me)
  
  # set parameters list according to current node's S0
  for i in range(NbDim):
      params[tune[i]] = str(S0[i])
  E0 = lau.cost(params, Compile = False)
  # get best results for all processes
  
  
  if 'annealing' in tuning_type:
    if 'tabu' in tuning_type:
      eb,Sb,iter = lau.simulatedAnnealingWithTabu(params, paramsVariationStep, tune, kmax, algoParameters["temp"], algoParameters["la"], algoParameters["ts"], MeId=Me)
    elif 'greedy' in tuning_type:
      eb,Sb,iter = lau.greedysimulatedAnnealing(params, paramsVariationStep, tune, kmax, algoParameters["temp"], algoParameters["la"], MeId=Me)
    elif "stage" in tuning_type:
      eb,Sb,iter = lau.simulatedAnnealingBasicStages(params, paramsVariationStep, tune, kmax, algoParameters["temp"], algoParameters["la"], algoParameters["sl"], MeId=Me)
    else: 
      eb,Sb,iter = lau.simulatedAnnealing(params, paramsVariationStep, tune, kmax, algoParameters["temp"], algoParameters["la"], MeId=Me)
  if 'greedy' in tuning_type:
    eb,Sb,iter = lau.onlyGreedyTuning(params, paramsVariationStep, tune, kmax, MeId=Me)
  if 'tabuDynamic' in tuning_type:
    eb,Sb,iter = lau.tabuTuningDynamicSize(params, paramsVariationStep, tune, kmax, algoParameters["tmin"], algoParameters["tmax"], MeId=Me)
  if 'tabu' in tuning_type:
    eb,Sb,iter = lau.tabuTuning(params,paramsVariationStep, tune, kmax, algoParameters["ts"], MeId=Me)
  else:
    eb,Sb,iter = lau.tuning(params, paramsVariationStep,tune, kmax, MeId=Me)

  #Process 0 (root) gathers results (Eb, Sb), Starting points (S0) and iter nb (Iter)
  # - Allocate real numpy arrays only on 0 (root) process
  if Me == 0:
    nd = len(tune)
    EbTab = np.zeros(NbP*1,dtype=np.float64) # energies
    SbTab = np.zeros(NbP*nd,dtype=int)       # best params
    S0Tab = np.zeros(NbP*nd,dtype=int)       # start params
    E0Tab = np.zeros(NbP*nd,dtype=int) 
    IterTab = np.zeros(NbP*1,dtype=int)      # TODO : ???
  else :
    EbTab   = None
    SbTab   = None
    S0Tab   = None
    E0Tab   = None
    IterTab = None
  # 112 64 [112, 68, 142] 142

  Eb = np.array([eb],dtype=np.float64)
  Sb = np.array([int(Sb[i]) for i in range(NbDim)])
  S0 = np.array([int(S0[i]) for i in range(NbDim)])
  E0 = np.array([E0],dtype=np.float64)
  Iter = np.array([iter],dtype=int)

  # gather all results
  comm.Gather(Eb, EbTab, root=0)
  comm.Gather(Sb, SbTab, root=0)
  comm.Gather(S0, S0Tab, root=0)
  comm.Gather(E0, E0Tab, root=0)
  comm.Gather(Iter, IterTab, root=0)

  # - Reduce all nbsim into totalNbSim                                    TO DO
  #totalNbSim = sim.GetNbSim()          # Replace this line with real reduction
  #totalNbSim = comm.reduce(sim.GetNbSim(),op=MPI.SUM,root=0)
  comm.barrier()
  time.sleep(2)
  #Print results
  #Print results
  if Me == 0:
  #    nd = hrst.GetNbDim()
  #    tools.printResults(EbTab,SbTab,S0Tab,IterTab,totalNbSim,nd,Me,NbP)

    # get the result with minimum energy
    Ebmin = min(EbTab)
    n = np.argmin(EbTab)
    Sbmin = SbTab[len(tune)*n:len(tune)*n+len(tune)]
    S0min = S0Tab[len(tune)*n:len(tune)*n+len(tune)]
    E0min = min(E0Tab)
    Itermin = IterTab[n]
    # print start and stop parameters for best opt.
    print("FINAL RESULTS --------------------------------------------------------------//")
    #print(Ebmin,n,Sbmin,S0min,Itermin)
    print(
      f"""
      RESULTS OF THE BEST PERFORMING RUN (with {tuning_type}):
      - Maximum throughput found: {Ebmin} (by process #{n} after {Itermin} iterations)
      - Starting parameters for tunable fields ({', '.join(tune)}):
        {S0min}
      - Best parameters for tunable fields:
        {Sbmin}
      """
    )
    #print("Press enter to exit, <y> and enter to print results from all processes.")
    #cmd = input()
    #if cmd == 'y':
    print(EbTab,SbTab,S0Tab,E0Tab,IterTab)
    print("\n")

    for i in range(NbDim):
        params[tune[i]] = str(Sbmin[i])
    lau.running(params["xNodeSize"], params["yNodeSize"], params["zNodeSize"], params["threading"], params["iteration"], params["xCacheBlocking"], params["yCacheBlocking"], params["zCacheBlocking"])


  #Process 0 prints a "good bye msg"
  if Me == 0:
      print("PE: ", Me, "/",NbP," bye!")
