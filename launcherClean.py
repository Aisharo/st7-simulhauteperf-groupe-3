import subprocess
import re
import sys
import math
import random as rd

from random import randrange

#----------------------------------------------------------------
# Compiling function: compile the application with the parameters
#----------------------------------------------------------------
def compiling(opt,simd):
  global SIMD
  SIMD = simd
  # - Check the parameters
  if opt not in ['-O2','-O3']:
      print('optimization illegal')
      return False
  if simd not in ['sse','avx','avx2','avx512']:
      print('simd illegal')
      return False
  # - Compile by make
  res = subprocess.run("make -e OPTIMIZATION=" + opt + ' -e simd=' + simd + ' last', shell=True, stdout=subprocess.PIPE)
  #print(str(res.stdout,'utf-8'))

#----------------------------------------------------------------
# running function: run the application with the parameters
#----------------------------------------------------------------
def running(n1,n2,n3,threads,iterations,tbn1,tbn2,tbn3,MeId=-1):
  # - run iso3dfd
  res = subprocess.run("./bin/iso3dfd_dev13_cpu_*" +' '+n1+' '+n2+' '+n3+' '+threads+' '+iterations+' '+tbn1+' '+tbn2+' '+tbn3, shell=True, stdout=subprocess.PIPE)
  print(f"<MeId: {MeId}>\n{str(res.stdout,'utf-8')}")
  return res

#----------------------------------------------------------------
# retrieving function: retrieve the output
#----------------------------------------------------------------
def retrieving(res):
  try:
    out=str(res.stdout,'utf-8')
    allout = re.findall(r'\d+\.?\d*',out)
    time = float(allout[-3])
    thrput = float(allout[-2])
    flops = float(allout[-1])
  except:
    print("@@ ERROR: Unparsable time!")
    thrput = 2147483647
  return thrput

#----------------------------------------------------------------
# neighbor function: find the neighbor of the S - NOT UP TO DATE -
#----------------------------------------------------------------
# def neighbor2(S):
#   Ln = []
#   N1 = S.copy()
#   N2 = S.copy()
#   N1[0] = str(int(N1[0]) + 16)
#   Ln.append(N1)
#   if int(N2[0])>16:
#       N2[0] = str(int(N2[0]) - 16)
#       Ln.append(N2)
#   for i in range(1,len(S)):
#       N1 = S.copy()
#       N2 = S.copy()
#       N1[i] = str(int(N1[i]) + 4)
#       Ln.append(N1)
#       if int(N2[i])>1:
#           N2[i] = str(int(N2[i]) - 4)
#           Ln.append(N2)
#   return Ln

#----------------------------------------------------------------
# neighbor function: find the neighbor of the S
#----------------------------------------------------------------
def neighbor(S, tune, paramsVariationStep):
  Ln = []

  for param in tune:
      N1 = S.copy()
      N2 = S.copy()
      p_index = tune.index(param)
      step = paramsVariationStep[param]
      N1[p_index] = str(int(N1[p_index]) + step)
      N2[p_index] = str(int(N2[p_index]) - step)
      Ln.append(N1)
      if int(N2[p_index]) > 0:
          Ln.append(N2)
  return Ln

#----------------------------------------------------------------
# cost function: calculate the cost
#----------------------------------------------------------------
def cost(params,Compile=True,MeId=-1):
  if Compile:
    compiling(params["compilationType"], params["makefileType"])
  res = running(params["xNodeSize"], params["yNodeSize"], params["zNodeSize"], params["threading"], params["iteration"], params["xCacheBlocking"], params["yCacheBlocking"], params["zCacheBlocking"], MeId=MeId)
  thrput = retrieving(res)
  return -thrput

#----------------------------------------------------------------
# tuning function: tune the parameters by hill climbing
#----------------------------------------------------------------
def tuning(params, paramsVariationStep, tune, kmax=3, MeId=-1):
  print("\n tune_launch:"+str(tune)+"\n")
  Sb = [params[param] for param in tune]
  Compile = False
  Eb = cost(params, Compile, MeId=MeId)
  Ln = neighbor(Sb, tune, paramsVariationStep)
  k = 0
  if ( "compilationType" and "makefileType" ) in tune:
      Compile = True       

  while (k < kmax and len(Ln)>0):
      n = randrange(0,len(Ln))
      S = Ln.pop(n)
      for param in tune:
          params[param] = S[tune.index(param)]
      E = cost(params, Compile, MeId=MeId)
      if E < Eb:
          Sb = S
          Eb = E
          Ln = neighbor(Sb, tune, paramsVariationStep)
      k = k+1
  return Eb,Sb,k

def onlyGreedyTuning(params, paramsVariationStep, tune, kmax, MeId=-1):
  Sb = [params[param] for param in tune]
  Compile = False
  Eb = cost(params, Compile, MeId=MeId)
  Ln = neighbor(Sb, tune, paramsVariationStep)
  k = 0
  newBetterS = True
  if ( "compilationType" and "makefileType" ) in tune:
      Compile = True      

  while (k < kmax and newBetterS):
      n = randrange(0,len(Ln))
      S = Ln.pop(n)
      for param in tune:
          params[param] = S[tune.index(param)]
      E = cost(params, Compile, MeId=MeId)
      for S2 in Ln :
          for param in tune:
              params[param] = S[tune.index(param)]
          E2 = cost(params, Compile, MeId=MeId)
          if E2 < E:
                S = S2
                E = E2
      if E < Eb:
          Sb = S
          Eb = E
          Ln = neighbor(Sb, tune, paramsVariationStep)
      else :
        newBetterS = False
      k = k+1
  return Eb,Sb,k
      


def findBest(params, Lneigh, Ltabu, tune, compile, MeId=-1): #Find the best Neigh not in the Tabu List
    E = sys.float_info.max
    S = None
    for Sneigh in Lneigh: 
        if Sneigh not in Ltabu:
            for param in tune:
              params[param] = Sneigh[tune.index(param)]
            Eneigh = cost(params, compile, MeId=MeId) #attention
            if (Eneigh < E):
                S = Sneigh
                E = Eneigh
    return S,E

def fifoAdd(Sbest, Ltabu, TabuSize):
    if len(Ltabu) == TabuSize:
        Ltabu.pop(0)
    Ltabu.append(Sbest)
    return Ltabu

def tabuTuning(params, paramsVariationStep, tune, kmax=3, TabuSize=3, MeId=-1):
  Sb = [params[param] for param in tune]
  for param in tune:
    params[param] = Sb[tune.index(param)]
  Compile = False
  Eb = cost(params, Compile, MeId=MeId)
  Ln = neighbor(Sb, tune, paramsVariationStep)
  Ltabu =[Sb]
  k = 0
  if ( "compilationType" or "makefileType" ) in tune:
      Compile = True
  while (k < kmax and len(Ln)>0):
      n = randrange(0,len(Ln))
      S, E = findBest(params,Ln, Ltabu, tune, Compile, MeId=MeId)
      Ln.pop(n)
      if E < Eb:
          Sb = S
          Eb = E
          Ltabu = fifoAdd(S, Ltabu, TabuSize)
          Ln = neighbor(Sb, tune, paramsVariationStep)
      k = k+1
  return Eb,Sb,k

# For now, min and max size will be calculated from size itself, but we may have another 2 entries 
def tabuTuningDynamicSize(params, paramsVariationStep, tune, kmax, minSize=3, maxSize = 15, MeId=-1) :
  size = (minSize + maxSize)//2
  Sb = [params[param] for param in tune]
  for param in tune:
    params[param] = Sb[tune.index(param)]
  Compile = False
  Eb = cost(params, Compile, MeId=MeId)
  Ln = neighbor(Sb, tune, paramsVariationStep)
  Ltabu =[Sb]
  k = 0
  if ( "compilationType" or "makefileType" ) in tune:
      Compile = True
  while (k < kmax and len(Ln)>0):
      n = randrange(0,len(Ln))
      S, E = findBest(params, Ln, Ltabu, tune, Compile, MeId=MeId)
      Ln.pop(n)
      if E < Eb:
          Sb = S
          Eb = E
          if size > minSize : 
            size -= 1
          Ltabu = fifoAdd(S, Ltabu, size)
          Ln = neighbor(Sb, tune, paramsVariationStep)
      elif size < maxSize : 
        size += 1
      k = k+1
  return Eb,Sb,k

#--------------------------------------------------------------------------
# Simulated Annealing
#--------------------------------------------------------------------------
#params: initial solution
#tune: keys of parameters to be tuned
#kmax: max nb of iteration
#T0: initial temperature for "simulated annealing"
#la: T = T*la: temperature evolution law for "simulated annealing"
def simulatedAnnealing(params, paramsVariationStep, tune, kmax, T0=100, la=0.85, MeId=-1):
    Sb = [params[param] for param in tune]
    Compile = False
    Eb = cost(params, Compile, MeId=MeId)
    iter = 0

    if ( "compilationType" and "makefileType" ) in tune:
        Compile = True   

    T = T0 

    #local search
    S, e, i = Sb, Eb, iter

    Ln = neighbor(S, tune, paramsVariationStep) # : generation of the neighbor list
    #We keep current values for simulated annhealing
    S_curr = Sb 
    E_curr = Eb
    # Note:
    for i in range(1, kmax+1):
        #We take the neighbors of our current value which is different from our candidate value.
        Ln = neighbor(S_curr, tune, paramsVariationStep)
        if not Ln:
          return Eb,Sb,iter
        k = randrange(len(Ln))
        S = Ln[k] # : remove and return the elt of rank k in mylist
        for param in tune:
            params[param] = S[tune.index(param)]
        # : run the simulator (or cost function) 
        E = cost(params, Compile, MeId=MeId)
        #    of index simIdx, on the solution S: evaluate the solution S.

        if E < Eb:
            Eb = E
            Sb = S
            iter = i
        diff = E-E_curr
        if diff <0 or rd.random() < math.exp( -(diff) / T ):
            S_curr,E_curr = S,E

        T *= la

    return Eb,Sb,iter

#--------------------------------------------------------------------------
# Simulated Annealing with basic stages
#--------------------------------------------------------------------------
#params: initial solution
#tune: keys of parameters to be tuned
#kmax: max nb of iteration
#T0: initial temperature for "simulated annealing"
#la: T = T*la: temperature evolution law for "simulated annealing"
#sl: Length of each simulated annealing stage
def simulatedAnnealingBasicStages(params, paramsVariationStep, tune, kmax, T0=100, la=0.85,sl=1, MeId=-1):
    Sb = [params[param] for param in tune]
    Compile = False
    Eb = cost(params, Compile, MeId=MeId)
    iter = 0

    if ( "compilationType" and "makefileType" ) in tune:
        Compile = True   

    T = T0 

    #local search
    S, e, i = Sb, Eb, iter

    Ln = neighbor(S, tune, paramsVariationStep) # : generation of the neighbor list
	
    stage = 1
    #We keep current values for simulated annhealing
    S_curr = Sb 
    E_curr = Eb
    # Note:
    for i in range(1, kmax+1):
        print("Stage " + str(stage) + "/" + str(sl) + " - T=" + str(T))
        print("k=" + str(i))
        #We take the neighbors of our current value which is different from our candidate value.
        Ln = neighbor(S_curr, tune, paramsVariationStep)
        if not Ln:
          return Eb,Sb,iter
        k = randrange(len(Ln))
        S = Ln[k] # : remove and return the elt of rank k in mylist
        for param in tune:
            params[param] = S[tune.index(param)]
        # : run the simulator (or cost function) 
        E = cost(params, Compile, MeId=MeId)
        #    of index simIdx, on the solution S: evaluate the solution S.
        
        if E < Eb:
            Eb = E
            Sb = S
            iter = i
        diff = E-E_curr
        if diff <0 or rd.random() < math.exp( -(diff) / T ):
            S_curr,E_curr = S,E
        
        stage+=1
        if stage >= sl:
            T *= la
            stage = 1

    return Eb,Sb,iter


#--------------------------------------------------------------------------
# Greedy Simulated Annealing
#--------------------------------------------------------------------------
#params: initial solution
#tune: keys of parameters to be tuned
#kmax: max nb of iteration
#T0: initial temperature for "simulated annealing"
#la: T = T*la: temperature evolution law for "simulated annealing"
def greedysimulatedAnnealing(params, paramsVariationStep, tune, kmax, T0=100, la=0.85, MeId=-1):
    Sb = [params[param] for param in tune]
    Compile = False
    Eb = cost(params, Compile, MeId=MeId)
    E = Eb
    iter = 0

    if ( "compilationType" and "makefileType" ) in tune:
        Compile = True

    T = T0

    #local search
    S, e, i = Sb, Eb, iter

    Ln = neighbor(S, tune, paramsVariationStep) # : generation of the neighbor list
    #We keep current values for simulated annhealing
    S_curr = Sb 
    E_curr = Eb
    # Note:
    for i in range(1, kmax+1):
        Ln = neighbor(S_curr, tune, paramsVariationStep)
        if not Ln:
          return Eb,Sb,iter
        #k = randrange(len(Ln))
        #S = Ln[k] # : remove and return the elt of rank k in mylist
        for Sneigh in Ln:
            for param in tune:
              params[param] = Sneigh[tune.index(param)]
            Eneigh = cost(params, Compile, MeId=MeId) #attention
            if (Eneigh < E):
                S = Sneigh
                E = Eneigh

        if E < Eb:
            Eb = E
            Sb = S
            iter = i
        diff = E-E_curr
        if diff <0 or rd.random() < math.exp( -(diff) / T ):
            S_curr,E_curr = S,E

        T *= la

    return Eb,Sb,iter

#--------------------------------------------------------------------------
# Simulated Annealing with Tabu List
#--------------------------------------------------------------------------
#params: initial solution
#tune: keys of parameters to be tuned
#kmax: max nb of iteration
#T0: initial temperature for "simulated annealing"
#la: T = T*la: temperature evolution law for "simulated annealing"
#TabuSize: length of the tabu list
def simulatedAnnealingWithTabu(params, paramsVariationStep, tune, kmax, T0=100, la=0.85, TabuSize=3, MeId=-1):
    Sb = [params[param] for param in tune]
    Compile = False
    Eb = cost(params, Compile, MeId=MeId)
    Ltabu =[Sb]
    iter = 0

    if ( "compilationType" and "makefileType" ) in tune:
        Compile = True   

    T = T0 

    #local search
    S, e, i = Sb, Eb, iter

    Ln = neighbor(S, tune, paramsVariationStep) # : generation of the neighbor list

    # Note:
    for i in range(1, kmax+1):
        if not Ln:
          return Eb,Sb,iter
        k = randrange(len(Ln))
        S, E = findBest(params, Ln, Ltabu, tune, Compile, MeId=MeId)
        Ln.pop(k)
        #    of index simIdx, on the solution S: evaluate the solution S.

        if E < Eb or rd.random() < math.exp( -(E - Eb) / T ):
            Eb = E
            Sb = S
            Ltabu = fifoAdd(S, Ltabu, TabuSize)
            Ln = neighbor(Sb, tune, paramsVariationStep)
            iter = i

        T *= la

    return Eb,Sb,iter


#-----------------------------------------------------------------
# Main code
#-----------------------------------------------------------------

# if __name__ == '__main__':

#     params = {"compilationType" : "-O3", "makefileType" : "avx512", "xNodeSize" : "256", "yNodeSize" : "256", "zNodeSize" : "256", "threading": "4", "iteration": "100", "xCacheBlocking" : "32", "yCacheBlocking" : "32", "zCacheBlocking" : "32"} 
#     paramsVariationStep = {"xNodeSize": 256, "yNodeSize": 256, "zNodeSize" : 256, "threading" : 1, "iteration": 1, "xCacheBlocking" : 16, "yCacheBlocking" : 4, "zCacheBlocking" : 4}
#     tune = ["xCacheBlocking", "yCacheBlocking", "zCacheBlocking"]
#     kmax = 10
#     compiling(params["compilationType"], params["makefileType"])
#     Sb=tuning(params, tune, kmax)
#     print(Sb)
