mode: parallel-random
n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.30 sec
throughput:   321.84 MPoints/s
flops:         19.63 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.23 sec
throughput:   327.11 MPoints/s
flops:         19.95 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.52 sec
throughput:   305.91 MPoints/s
flops:         18.66 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.19 sec
throughput:   329.61 MPoints/s
flops:         20.11 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.54 sec
throughput:   304.37 MPoints/s
flops:         18.57 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.15 sec
throughput:   332.98 MPoints/s
flops:         20.31 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=44
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.51 sec
throughput:   306.24 MPoints/s
flops:         18.68 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.15 sec
throughput:   333.02 MPoints/s
flops:         20.31 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.38 sec
throughput:   315.42 MPoints/s
flops:         19.24 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.17 sec
throughput:   331.51 MPoints/s
flops:         20.22 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.05 sec
throughput:   340.98 MPoints/s
flops:         20.80 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=44
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.51 sec
throughput:   306.58 MPoints/s
flops:         18.70 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.07 sec
throughput:   339.31 MPoints/s
flops:         20.70 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.21 sec
throughput:   328.30 MPoints/s
flops:         20.03 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.04 sec
throughput:   342.31 MPoints/s
flops:         20.88 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.29 sec
throughput:   322.36 MPoints/s
flops:         19.66 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.05 sec
throughput:   341.65 MPoints/s
flops:         20.84 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=100 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.45 sec
throughput:   310.79 MPoints/s
flops:         18.96 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.17 sec
throughput:   331.72 MPoints/s
flops:         20.24 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.05 sec
throughput:   341.02 MPoints/s
flops:         20.80 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=104 n3_thrd_block=44
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.50 sec
throughput:   306.87 MPoints/s
flops:         18.72 GFlops

mode: parallel-random
PE:  0 / 2 : all processes started
n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.88 sec
throughput:   283.17 MPoints/s
flops:         17.27 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.63 sec
throughput:   298.34 MPoints/s
flops:         18.20 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.70 sec
throughput:   293.86 MPoints/s
flops:         17.93 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=72 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.94 sec
throughput:   279.81 MPoints/s
flops:         17.07 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.77 sec
throughput:   290.07 MPoints/s
flops:         17.69 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.89 sec
throughput:   282.80 MPoints/s
flops:         17.25 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=140
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.81 sec
throughput:   287.11 MPoints/s
flops:         17.51 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=64 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.70 sec
throughput:   294.17 MPoints/s
flops:         17.94 GFlops

FINAL RESULTS --------------------------------------------------------------//

      RESULTS OF THE BEST PERFORMING RUN (with parallel-random):
      - Minimum energy found: 4.04 (by process #1 after 20 iterations)
      - Starting parameters for tunable fields (xCacheBlocking, yCacheBlocking, zCacheBlocking):
        [160 112  48]
      - Final parameters for tunable fields:
        [144 104  48]
      
Press enter to exit, <y> and enter to print results from all processes.
[4.63 4.04] [128  68 144 144 104  48] [112  68 144 160 112  48] [ 7 20]
PE:  0 / 2  bye!
