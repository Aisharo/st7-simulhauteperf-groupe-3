#####################################################
INITIAL MAKE CALLED: with -O3 avx512
#####################################################
n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=272 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.40 sec
throughput:    89.75 MPoints/s
flops:          5.47 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.38 sec
throughput:    89.86 MPoints/s
flops:          5.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=272 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.46 sec
throughput:    89.40 MPoints/s
flops:          5.45 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.59 sec
throughput:    88.69 MPoints/s
flops:          5.41 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=268 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          16.33 sec
throughput:    84.68 MPoints/s
flops:          5.17 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=272 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.58 sec
throughput:    88.71 MPoints/s
flops:          5.41 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=272 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.59 sec
throughput:    88.65 MPoints/s
flops:          5.41 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=272 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.56 sec
throughput:    88.82 MPoints/s
flops:          5.42 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.72 sec
throughput:    87.96 MPoints/s
flops:          5.37 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=268 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.55 sec
throughput:    88.91 MPoints/s
flops:          5.42 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.68 sec
throughput:    88.14 MPoints/s
flops:          5.38 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          16.66 sec
throughput:    82.99 MPoints/s
flops:          5.06 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.22 sec
throughput:    90.83 MPoints/s
flops:          5.54 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=268 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.35 sec
throughput:    90.07 MPoints/s
flops:          5.49 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.66 sec
throughput:    88.28 MPoints/s
flops:          5.39 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.81 sec
throughput:    87.43 MPoints/s
flops:          5.33 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.70 sec
throughput:    88.06 MPoints/s
flops:          5.37 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.52 sec
throughput:    89.06 MPoints/s
flops:          5.43 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.39 sec
throughput:    89.85 MPoints/s
flops:          5.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.31 sec
throughput:    90.27 MPoints/s
flops:          5.51 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.31 sec
throughput:    90.27 MPoints/s
flops:          5.51 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.40 sec
throughput:    89.76 MPoints/s
flops:          5.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.65 sec
throughput:    88.31 MPoints/s
flops:          5.39 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.31 sec
throughput:    90.32 MPoints/s
flops:          5.51 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.71 sec
throughput:    88.00 MPoints/s
flops:          5.37 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.24 sec
throughput:    90.71 MPoints/s
flops:          5.53 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.69 sec
throughput:    88.09 MPoints/s
flops:          5.37 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.51 sec
throughput:    89.12 MPoints/s
flops:          5.44 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=564
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.47 sec
throughput:    89.36 MPoints/s
flops:          5.45 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.69 sec
throughput:    88.11 MPoints/s
flops:          5.37 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.49 sec
throughput:    89.22 MPoints/s
flops:          5.44 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.33 sec
throughput:    90.17 MPoints/s
flops:          5.50 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.29 sec
throughput:    90.43 MPoints/s
flops:          5.52 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=276 n3_thrd_block=572
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.51 sec
throughput:    89.13 MPoints/s
flops:          5.44 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=280 n3_thrd_block=568
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.35 sec
throughput:    90.04 MPoints/s
flops:          5.49 GFlops
