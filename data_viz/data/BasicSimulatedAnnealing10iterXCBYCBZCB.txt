mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=48 n2_thrd_block=144 n3_thrd_block=152
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.05 sec
throughput:    67.40 MPoints/s
flops:          4.11 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=48 n2_thrd_block=140 n3_thrd_block=152
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.05 sec
throughput:    67.34 MPoints/s
flops:          4.11 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=140 n3_thrd_block=152
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.00 sec
throughput:    69.06 MPoints/s
flops:          4.21 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=48 n2_thrd_block=140 n3_thrd_block=152
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.89 sec
throughput:    73.15 MPoints/s
flops:          4.46 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=20 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           0.99 sec
throughput:   139.31 MPoints/s
flops:          8.50 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=24 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.23 sec
throughput:   112.54 MPoints/s
flops:          6.86 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=28 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.25 sec
throughput:   110.61 MPoints/s
flops:          6.75 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=28 n3_thrd_block=44
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.12 sec
throughput:   123.89 MPoints/s
flops:          7.56 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=144 n3_thrd_block=20
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.95 sec
throughput:    71.07 MPoints/s
flops:          4.34 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=144 n3_thrd_block=20
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.91 sec
throughput:    72.21 MPoints/s
flops:          4.40 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=148 n3_thrd_block=20
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.86 sec
throughput:    74.16 MPoints/s
flops:          4.52 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=152 n3_thrd_block=20
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.72 sec
throughput:    80.15 MPoints/s
flops:          4.89 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=48 n3_thrd_block=60
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.65 sec
throughput:    83.97 MPoints/s
flops:          5.12 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=48 n3_thrd_block=60
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.59 sec
throughput:    86.81 MPoints/s
flops:          5.30 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=48 n3_thrd_block=60
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.63 sec
throughput:    84.83 MPoints/s
flops:          5.17 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=44 n3_thrd_block=60
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.49 sec
throughput:    92.84 MPoints/s
flops:          5.66 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=80 n3_thrd_block=116
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.88 sec
throughput:    73.59 MPoints/s
flops:          4.49 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=80 n3_thrd_block=112
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.86 sec
throughput:    74.51 MPoints/s
flops:          4.55 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=80 n3_thrd_block=108
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.70 sec
throughput:    81.28 MPoints/s
flops:          4.96 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=84 n3_thrd_block=108
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.74 sec
throughput:    79.47 MPoints/s
flops:          4.85 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=48 n2_thrd_block=100 n3_thrd_block=24
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.78 sec
throughput:    77.69 MPoints/s
flops:          4.74 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=100 n3_thrd_block=24
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.94 sec
throughput:    71.26 MPoints/s
flops:          4.35 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=104 n3_thrd_block=24
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.90 sec
throughput:    72.94 MPoints/s
flops:          4.45 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=104 n3_thrd_block=20
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.83 sec
throughput:    75.70 MPoints/s
flops:          4.62 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=60 n3_thrd_block=76
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.61 sec
throughput:    85.98 MPoints/s
flops:          5.24 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=60 n3_thrd_block=76
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.66 sec
throughput:    83.37 MPoints/s
flops:          5.09 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=60 n3_thrd_block=72
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.64 sec
throughput:    84.49 MPoints/s
flops:          5.15 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=60 n3_thrd_block=76
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.55 sec
throughput:    89.07 MPoints/s
flops:          5.43 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=152 n3_thrd_block=132
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.18 sec
throughput:    63.44 MPoints/s
flops:          3.87 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=156 n3_thrd_block=132
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.77 sec
throughput:    49.93 MPoints/s
flops:          3.05 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=156 n3_thrd_block=132
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.44 sec
throughput:    56.65 MPoints/s
flops:          3.46 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=156 n3_thrd_block=128
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.82 sec
throughput:    76.02 MPoints/s
flops:          4.64 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.87 sec
throughput:    73.82 MPoints/s
flops:          4.50 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.92 sec
throughput:    72.05 MPoints/s
flops:          4.40 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.81 sec
throughput:    76.40 MPoints/s
flops:          4.66 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.62 sec
throughput:    85.15 MPoints/s
flops:          5.19 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=48 n2_thrd_block=80 n3_thrd_block=36
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.81 sec
throughput:    76.40 MPoints/s
flops:          4.66 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=80 n3_thrd_block=36
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.78 sec
throughput:    77.52 MPoints/s
flops:          4.73 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=80 n3_thrd_block=40
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.72 sec
throughput:    80.48 MPoints/s
flops:          4.91 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=64 n2_thrd_block=84 n3_thrd_block=40
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.69 sec
throughput:    81.68 MPoints/s
flops:          4.98 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=28 n3_thrd_block=92
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.15 sec
throughput:   119.89 MPoints/s
flops:          7.31 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=28 n3_thrd_block=96
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.30 sec
throughput:   106.22 MPoints/s
flops:          6.48 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=24 n3_thrd_block=96
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.21 sec
throughput:   114.69 MPoints/s
flops:          7.00 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=24 n3_thrd_block=92
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.11 sec
throughput:   124.40 MPoints/s
flops:          7.59 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=140 n3_thrd_block=116
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.96 sec
throughput:    70.56 MPoints/s
flops:          4.30 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=140 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.07 sec
throughput:    66.83 MPoints/s
flops:          4.08 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=144 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.21 sec
throughput:    62.60 MPoints/s
flops:          3.82 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=148 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.82 sec
throughput:    76.00 MPoints/s
flops:          4.64 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=108 n3_thrd_block=76
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.97 sec
throughput:    70.05 MPoints/s
flops:          4.27 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=108 n3_thrd_block=72
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.97 sec
throughput:    70.08 MPoints/s
flops:          4.27 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=16 n2_thrd_block=108 n3_thrd_block=72
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.20 sec
throughput:    62.77 MPoints/s
flops:          3.83 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=16 n2_thrd_block=104 n3_thrd_block=72
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           2.04 sec
throughput:    67.91 MPoints/s
flops:          4.14 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=136 n3_thrd_block=40
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.99 sec
throughput:    69.32 MPoints/s
flops:          4.23 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=136 n3_thrd_block=36
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.96 sec
throughput:    70.41 MPoints/s
flops:          4.29 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=140 n3_thrd_block=36
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.89 sec
throughput:    73.11 MPoints/s
flops:          4.46 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=144 n3_thrd_block=36
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.73 sec
throughput:    79.72 MPoints/s
flops:          4.86 GFlops

mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=36 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.25 sec
throughput:   110.19 MPoints/s
flops:          6.72 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=80 n2_thrd_block=32 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.36 sec
throughput:   101.81 MPoints/s
flops:          6.21 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=32 n3_thrd_block=120
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.43 sec
throughput:    96.34 MPoints/s
flops:          5.88 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=32 n3_thrd_block=116
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.47 sec
throughput:    93.95 MPoints/s
flops:          5.73 GFlops

mode: parallel-random-simulated-annhealing
PE:  0 / 16 : all processes started
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.74 sec
throughput:    79.63 MPoints/s
flops:          4.86 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.77 sec
throughput:    77.92 MPoints/s
flops:          4.75 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.80 sec
throughput:    76.62 MPoints/s
flops:          4.67 GFlops

n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           1.62 sec
throughput:    85.44 MPoints/s
flops:          5.21 GFlops

FINAL RESULTS --------------------------------------------------------------//

      RESULTS OF THE BEST PERFORMING RUN (with parallel-random-simulated-annhealing):
      - Minimum energy found: 0.99 (by process #9 after 0 iterations)
      - Starting parameters for tunable fields (xCacheBlocking, yCacheBlocking, zCacheBlocking):
        [144  20  48]
      - Final parameters for tunable fields:
        [144  20  48]

Press enter to exit, <y> and enter to print results from all processes.
y
[1.62 1.62 1.69 1.11 1.82 1.97 1.73 1.25 1.89 0.99 1.72 1.49 1.7  1.78
 1.55 1.82] [128  68 144 160 112  52  64  84  40 112  24  92 128 148 120  32 108  76
 160 144  36  80  36 120  48 140 152 144  20  48  64 152  20 128  44  60
 144  80 108  48 100  24  80  60  76 144 156 128] [112  68 144 160 112  48  48  80  36 112  28  92 128 140 116  32 108  76
 160 136  40  80  36 120  48 144 152 144  20  48  80 144  20 160  48  60
 144  80 116  48 100  24  96  60  76 160 152 132] [3 3 3 3 3 0 3 0 3 0 3 3 2 0 3 3]
PE:  0 / 16  bye!
