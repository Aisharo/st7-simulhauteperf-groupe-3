n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=272 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.88 sec
throughput:    92.90 MPoints/s
flops:          5.67 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.84 sec
throughput:    93.16 MPoints/s
flops:          5.68 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=272 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.86 sec
throughput:    87.18 MPoints/s
flops:          5.32 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=276 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.99 sec
throughput:    92.22 MPoints/s
flops:          5.63 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=268 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.71 sec
throughput:    94.01 MPoints/s
flops:          5.73 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=268 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.63 sec
throughput:    94.48 MPoints/s
flops:          5.76 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=96 n2_thrd_block=268 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          15.56 sec
throughput:    88.82 MPoints/s
flops:          5.42 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=264 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.73 sec
throughput:    93.88 MPoints/s
flops:          5.73 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=268 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.77 sec
throughput:    93.62 MPoints/s
flops:          5.71 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=272 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          14.73 sec
throughput:    93.82 MPoints/s
flops:          5.72 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=264 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:          16.15 sec
throughput:    85.61 MPoints/s
flops:          5.22 GFlops


