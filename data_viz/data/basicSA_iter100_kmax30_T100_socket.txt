mode: parallel-random-simulated-annhealing
n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.25 sec
throughput:   325.07 MPoints/s
flops:         19.83 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.38 sec
throughput:   315.67 MPoints/s
flops:         19.26 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.60 sec
throughput:   300.67 MPoints/s
flops:         18.34 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.53 sec
throughput:   305.35 MPoints/s
flops:         18.63 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.45 sec
throughput:   310.34 MPoints/s
flops:         18.93 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=192 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.43 sec
throughput:   312.00 MPoints/s
flops:         19.03 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=192 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.51 sec
throughput:   306.50 MPoints/s
flops:         18.70 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.53 sec
throughput:   304.93 MPoints/s
flops:         18.60 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=116 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.57 sec
throughput:   302.48 MPoints/s
flops:         18.45 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.56 sec
throughput:   302.93 MPoints/s
flops:         18.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=112 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.47 sec
throughput:   309.16 MPoints/s
flops:         18.86 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=108 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.75 sec
throughput:   290.82 MPoints/s
flops:         17.74 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.23 sec
throughput:   326.91 MPoints/s
flops:         19.94 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.14 sec
throughput:   334.13 MPoints/s
flops:         20.38 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=108 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.38 sec
throughput:   315.75 MPoints/s
flops:         19.26 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.26 sec
throughput:   324.45 MPoints/s
flops:         19.79 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=56
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.44 sec
throughput:   311.53 MPoints/s
flops:         19.00 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.27 sec
throughput:   323.97 MPoints/s
flops:         19.76 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.11 sec
throughput:   336.53 MPoints/s
flops:         20.53 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=100 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           3.97 sec
throughput:   347.80 MPoints/s
flops:         21.22 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=100 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.17 sec
throughput:   331.51 MPoints/s
flops:         20.22 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=100 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.05 sec
throughput:   341.64 MPoints/s
flops:         20.84 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=48
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.16 sec
throughput:   332.68 MPoints/s
flops:         20.29 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=44
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.61 sec
throughput:   300.02 MPoints/s
flops:         18.30 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.92 sec
throughput:   280.76 MPoints/s
flops:         17.13 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.25 sec
throughput:   325.56 MPoints/s
flops:         19.86 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=104 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.26 sec
throughput:   324.55 MPoints/s
flops:         19.80 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=100 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.16 sec
throughput:   332.01 MPoints/s
flops:         20.25 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=100 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.13 sec
throughput:   334.93 MPoints/s
flops:         20.43 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=96 n3_thrd_block=52
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.02 sec
throughput:   343.73 MPoints/s
flops:         20.97 GFlops
mode: parallel-random-simulated-annhealing
PE:  0 / 2 : all processes started
n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=112 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.83 sec
throughput:   286.11 MPoints/s
flops:         17.45 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.78 sec
throughput:   289.08 MPoints/s
flops:         17.63 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.72 sec
throughput:   292.84 MPoints/s
flops:         17.86 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.63 sec
throughput:   298.67 MPoints/s
flops:         18.22 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.73 sec
throughput:   292.56 MPoints/s
flops:         17.85 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.70 sec
throughput:   293.86 MPoints/s
flops:         17.93 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.63 sec
throughput:   298.62 MPoints/s
flops:         18.22 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=64 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.80 sec
throughput:   287.93 MPoints/s
flops:         17.56 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=64 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.82 sec
throughput:   286.64 MPoints/s
flops:         17.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=64 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.76 sec
throughput:   290.12 MPoints/s
flops:         17.70 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=64 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.55 sec
throughput:   303.59 MPoints/s
flops:         18.52 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=144
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.76 sec
throughput:   290.71 MPoints/s
flops:         17.73 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=128 n2_thrd_block=68 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.79 sec
throughput:   288.51 MPoints/s
flops:         17.60 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.01 sec
throughput:   275.71 MPoints/s
flops:         16.82 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=72 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.88 sec
throughput:   283.47 MPoints/s
flops:         17.29 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=148
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.99 sec
throughput:   276.93 MPoints/s
flops:         16.89 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=152
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.96 sec
throughput:   278.52 MPoints/s
flops:         16.99 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=144 n2_thrd_block=68 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.07 sec
throughput:   272.43 MPoints/s
flops:         16.62 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=160 n2_thrd_block=68 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.08 sec
throughput:   272.08 MPoints/s
flops:         16.60 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=68 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.10 sec
throughput:   270.97 MPoints/s
flops:         16.53 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=68 n3_thrd_block=160
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.22 sec
throughput:   264.90 MPoints/s
flops:         16.16 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=192 n2_thrd_block=68 n3_thrd_block=160
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.26 sec
throughput:   262.75 MPoints/s
flops:         16.03 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=68 n3_thrd_block=160
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.82 sec
throughput:   237.39 MPoints/s
flops:         14.48 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=68 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.05 sec
throughput:   273.55 MPoints/s
flops:         16.69 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=64 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.85 sec
throughput:   285.14 MPoints/s
flops:         17.39 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=60 n3_thrd_block=156
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.08 sec
throughput:   272.32 MPoints/s
flops:         16.61 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=60 n3_thrd_block=160
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.90 sec
throughput:   281.99 MPoints/s
flops:         17.20 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=64 n3_thrd_block=160
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.05 sec
throughput:   273.89 MPoints/s
flops:         16.71 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=64 n3_thrd_block=164
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.37 sec
throughput:   257.63 MPoints/s
flops:         15.72 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=68 n3_thrd_block=164
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           5.36 sec
throughput:   257.73 MPoints/s
flops:         15.72 GFlops

n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=176 n2_thrd_block=96 n3_thrd_block=56
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.19 sec
throughput:   329.94 MPoints/s
flops:         20.13 GFlops


n1=256 n2=256 n3=256 nreps=100 num_threads=4 HALF_LENGTH=8
n1_thrd_block=208 n2_thrd_block=64 n3_thrd_block=164
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           4.98 sec
throughput:   277.32 MPoints/s
flops:         16.92 GFlops

FINAL RESULTS --------------------------------------------------------------//

      RESULTS OF THE BEST PERFORMING RUN (with parallel-random-simulated-annhealing):
      - Minimum energy found: 3.97 (by process #1 after 19 iterations)
      - Starting parameters for tunable fields (xCacheBlocking, yCacheBlocking, zCacheBlocking):
        [160 112  48]
      - Final parameters for tunable fields:
        [160 100  48]
      
Press enter to exit, <y> and enter to print results from all processes.
[4.55 3.97] [128  64 144 160 100  48] [112  68 144 160 112  48] [10 19]
PE:  0 / 2  bye!
