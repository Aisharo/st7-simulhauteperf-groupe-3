cpust75_1@kyle02:~/Proj-Intel/Appli-iso3dfd$  mpirun -np 1 -map-by ppr:1:core -bind-to core bin/iso3dfd_dev13_cpu_avx512.exe 256 256 256 4 10 32 32 32
n1=256 n2=256 n3=256 nreps=10 num_threads=4 HALF_LENGTH=8
n1_thrd_block=32 n2_thrd_block=32 n3_thrd_block=32
allocating prev, next and vel: total 192 Mbytes
-------------------------------
time:           0.40 sec
throughput:   348.30 MPoints/s
flops:         21.25 GFlops
